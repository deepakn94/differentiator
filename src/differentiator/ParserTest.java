package differentiator;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class ParserTest {

	//Introduced more simple tests to reflect code review comments
	
	@Test
	public void simpleTestSumExpression() {
		Lexer Lexer = new Lexer("(1+1)");
		Parser Parser = new Parser(Lexer);
		
		Expression result = Parser.returnExpression();
		String expectedResult = "Sum(Constant(1),Constant(1))";
		assertEquals(expectedResult, result.toString());
	}
	
	@Test
	public void simpleTestProductExpression() {
		Lexer Lexer = new Lexer("(y*x)");
		Parser Parser = new Parser(Lexer);
		
		Expression result = Parser.returnExpression();
		String expectedResult = "Product(Variable(y),Variable(x))";
		assertEquals(expectedResult, result.toString());
	} 
	
	@Test
	public void simpleTestVariableExpression() {
		Lexer Lexer = new Lexer("(x)");
		Parser Parser = new Parser(Lexer);
		
		Expression result = Parser.returnExpression();
		String expectedResult = "Variable(x)";
		assertEquals(expectedResult, result.toString());
	}
	
	@Test
	public void simpleTestConstantExpression() {
		Lexer Lexer = new Lexer("(3.0)");
		Parser Parser = new Parser(Lexer);
		
		Expression result = Parser.returnExpression();
		String expectedResult = "Constant(3.0)";
		assertEquals(expectedResult, result.toString());
	} 
	
	@Test
	public void longTestReturnExpression() {
		Lexer Lexer = new Lexer("((((a + b) + 3) * d) + (4.0 * f))");
		Parser Parser = new Parser(Lexer);
		
		Expression result = Parser.returnExpression();
		String expectedResult = "Sum(Product(Sum(Sum(Variable(a),Variable(b)),Constant(3)),Variable(d)),Product(Constant(4.0),Variable(f)))";
		assertEquals(expectedResult, result.toString());
	}
	
	@Test
	public void manyParentheses() {
		Lexer Lexer = new Lexer("(((x + 1.0)+y))");
		Parser Parser = new Parser(Lexer);
		
		Expression result = Parser.returnExpression();
		String expectedResult = "Sum(Sum(Variable(x),Constant(1.0)),Variable(y))";
		assertEquals(expectedResult, result.toString());
	}
	
	@Test(expected = RuntimeException.class)
	public void testTooManyCloseParentheses() {
		Lexer Lexer = new Lexer("(((x + 1.0)*5.0)))");
		Parser Parser = new Parser(Lexer);
		
		Parser.returnExpression();
	}

	@Test(expected = RuntimeException.class)
	public void testTooManyOpenParentheses() {
		Lexer Lexer = new Lexer("(((x + 1.0)*5.0)");
		Parser Parser = new Parser(Lexer);
		
		Parser.returnExpression();
	}
	
	@Test(expected = RuntimeException.class)
	public void testConsecutiveOperands() {
		Lexer Lexer = new Lexer("(x 1.0 +)");
		Parser Parser = new Parser(Lexer);
		
		Parser.returnExpression();
	}
}

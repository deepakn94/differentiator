package differentiator;

import java.util.Stack;

import differentiator.Token.Type;

/*
 * Terminals
 * Constant ::= [0-9]+ | [0-9]+.[0-9]+
 * Variable ::= [A-Za-z]+
 * Operator ::= + | *
 * OpenParenthesis ::= (
 * CloseParentheses ::= )
 * 
 * 
 * Grammar
 * 
 * ParenthesizedExpression ::= OpenParenthesis Expression CloseParenthesis
 * Expression ::= [Expression Constant Variable] Operator [Expression Constant Variable] | ParenthesizedExpression
 * 
 */

/**
 * The parser gets a bunch of tokens from the lexer and determines what
 * expression was written by the user.
 */
public class Parser {
    /**
     * Creates a new parser over the lexer.  This parser will use the passed
     * lexer to get tokens--which it will then parse.
     * @param lexer The lexer.
     */
	private final Lexer lexer;
	
    public Parser(Lexer lexer) {
        this.lexer = lexer;
    }
    
    //Retained the name ASTERISK because I am using operator names and not expression names
    public static enum Operator {
        PLUS,
        ASTERISK,
        OPEN_PARENTHESIS,
        CLOSE_PARENTHESIS
    }
    
    /**
     * Returns the parsed expression equivalent to the expression input by the user
     * <p>
     * Takes in tokens from the lexer
     * @return an instance of type Expression, representing the parsed expression
     */
    public Expression returnExpression() {
    	/*
    	 * I use two stacks to correctly parse the inputed expression - an expression stack and an operator stack
    	 * All operators/parentheses are pushed onto the operator stack
    	 * All expressions are pushed onto the expression stack
    	 * Every time a close parenthesis is encountered, two expressions are popped out from the expression stack, 
    	 * the operator is popped from the operator stack, and the corresponding result is pushed onto the expression
    	 * stack. 
    	 * This is done for each token in the tokenized expression.
    	 */
    	lexer.reset(); //Reset the lexer before we start tokenization
    	Token nextToken = lexer.next();
    	
    	Stack<Expression> expressionStack = new Stack<Expression> ();
    	Stack<Operator> operatorStack = new Stack<Operator> ();
    	boolean lastTokenExpression = false;
    	boolean lastTokenOperator = false;
    	while (nextToken.getTokentype()!=Type.EOE) {
    		//Changed to switch case after looking at code review comments
    		switch(nextToken.getTokentype()) {
    		
	    		case EOE:
	    			break;
	    			
	    		case OPEN_PARENTHESIS: 
	    			lastTokenExpression = false;
	    			lastTokenOperator = false;
	    			operatorStack.push(Operator.OPEN_PARENTHESIS);
	    			break;
	    			
	    		case PLUS: 
	    			if (lastTokenOperator == true) { throw new RuntimeException("Incorrect Expression format: Consecutive operators"); }
	    			lastTokenOperator = true;
	    			lastTokenExpression = false;
	    			operatorStack.push(Operator.PLUS);	
	    			break;
	    			
	    		case MULTIPLY: 
	    			if (lastTokenOperator == true) { throw new RuntimeException("Incorrect Expression format: Consecutive operators"); }
	    			lastTokenOperator = true;
	    			lastTokenExpression = false;
	    			operatorStack.push(Operator.ASTERISK);	
	    			break;
	    			
	    		case CONSTANT:
	    			if (lastTokenExpression == true) { throw new RuntimeException("Incorrect Expression format: Consecutive expressions"); }
	    			lastTokenExpression = true;
	    			lastTokenOperator = false;
	    			expressionStack.push(new Constant(nextToken.getValue()));	
	    			break;
	    			
	    		case VARIABLE:
	    			if (lastTokenExpression == true) { throw new RuntimeException("Incorrect Expression format: Consecutive expressions"); }
	    			lastTokenExpression = true;
	    			lastTokenOperator = false;
	    			expressionStack.push(new Variable(nextToken.getValue()));	
	    			break;
	    			
	    		case CLOSE_PARENTHESIS: 
	    			lastTokenExpression = false;
	    			lastTokenOperator = false;
	    			if (expressionStack.isEmpty() || operatorStack.isEmpty()) { 
	    				throw new RuntimeException("Incorrect Expression format");
	    			}
	    			
	    			Operator operator = operatorStack.peek();
	    			if (operator.equals(Operator.ASTERISK)) {
	    				operatorStack.pop();
	    				Expression operand1 = expressionStack.pop();
	    				Expression operand2 = expressionStack.pop();
	    				//This ensures that the product is "evaluated"
	    				Expression product = new Product(operand2, operand1);
	    				expressionStack.push(product);
	    			}
	    			else if (operator.equals(Operator.PLUS)) {
	    				operatorStack.pop();
	    				Expression operand1 = expressionStack.pop();
	    				Expression operand2 = expressionStack.pop();
	    				//This ensures that the sum is "evaluated"
	    				Expression sum = new Sum(operand2, operand1);
	    				expressionStack.push(sum);
	    			}
	    			
	    			if (operatorStack.peek().equals(Operator.OPEN_PARENTHESIS)) { operatorStack.pop(); }
	    			break;
    		}
    		
    		nextToken = lexer.next();
    	}
    	Expression result = expressionStack.pop();
    	if (! expressionStack.empty()) { throw new RuntimeException("Incorrect Expression format"); }
    	if (! operatorStack.empty()) { throw new RuntimeException("Incorrect Expression format"); }    	
		return result;
    }
    
}

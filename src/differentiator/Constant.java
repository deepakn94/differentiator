package differentiator;

/**
 * Class Constant that implements the interface Expression
 * <p>
 * Represents all constant terms in an Expression
 * @author Deepak
 *
 */
public class Constant implements Expression {
	private final String value;
	
	/**
	 * @param value The constant value that is represented by the Constant object
	 */
	public Constant(String value) {
		this.value = value;
	}
	
	public String getValue() {
		return value;
	}
	
	/*
	 * This method is used purely for the purpose of debugging/testing, because it gives a good idea
	 * of whether parsing is being done correctly or not
	 */
	@Override
	public String toString() {
		return "Constant(" + value + ")";
	}
	
	public <R> R accept(Visitor<R> v) {
		return v.on(this);
	}
	
	//Throws an exception since Constant doesn't have a first operand
	public Expression getFirstOperand() {
		throw new UnsupportedOperationException("Constant does not have a first operand!");
	}
	
	//Throws an exception since Constant doesn't have a second operand
	public Expression getSecondOperand() {
		throw new UnsupportedOperationException("Constant does not have a second operand!");
	}
	
	/*
	 * Returns a string that represents the constant in a neat format, that is 
	 * used to display the final differentiated expression  
	 */
	public String toSimplifiedNotation() {
		return value;
	}

	/*
	 * Returns a simplified polynomial expression of a constant
	 * (non-Javadoc)
	 * @see differentiator.Expression#getSimplifiedPolynomial()
	 */
	@Override
	public SimplifiedPolynomial getSimplifiedPolynomial() {
		double coefficient = Double.valueOf(value);
		SimplifiedTerm simplifiedTerm = new SimplifiedTerm(coefficient, null);
		SimplifiedPolynomial simplifiedPolynomial = new SimplifiedPolynomial(simplifiedTerm);
		return simplifiedPolynomial;
	}
}

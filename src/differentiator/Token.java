package differentiator;

/**
 * A token is a lexical item that the parser uses.
 */
public class Token {
    /**
     * All the types of tokens that can be made.
     * 
     * Different types of tokens used are :-
     * Constant numbers, 
     * String variable names, 
     * Open parenthesis, 
     * Close parenthesis, 
     * Plus sign, 
     * Multiply sign, 
     * End of Expression
     * 
     * <p>
     * 
     * Different groupings :-
     * Constant numbers, 
     * String variable names, 
     * Open parenthesis, 
     * Close parenthesis, 
     * Addition sign, 
     * Multiplication sign, 
     * End of Expression
     */
    public static enum Type {
        VARIABLE, // Variable name, that contains only alphabets
        CONSTANT, // Numerical constant
        
        OPEN_PARENTHESIS, // (
        CLOSE_PARENTHESIS, // )
        
        PLUS, // +
        MULTIPLY, // *
        
        EOE //End of Expression

    }
    
    private final Type tokenType;
    private final String value;

    public Token(Type tokenType, String value) {
    	this.tokenType = tokenType;
    	this.value = value;
    }
    
    /**
     * Observer method for the Token class
     * @return A String that represents the string value of the token
     */
    public String getValue() {
    	return this.value;
    }
    
    /**
     * Observer method of the Token class
     * @return An object of the enum Type that represents the type of token
     */
    public Type getTokentype() {
    	return this.tokenType;
    }
    
}
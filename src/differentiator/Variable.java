package differentiator;

/**
 * Class Variable that implements the interface Expression
 * <p>
 * Represents all variable terms in an Expression
 * @author Deepak
 *
 */
public class Variable implements Expression {
	private final String value;
	
	/**
	 * @param value The variable name value that is represented by the Variable object
	 */
	public Variable(String value) {
		this.value = value;
	}
	
	public String getValue() {
		return value;
	}
	
	/*
	 * This method is used purely for the purpose of debugging/testing, because it gives a good idea
	 * of whether parsing is being done correctly or not
	 */
	@Override
	public String toString() {
		return "Variable("+value+")";
	}
	
	public <R> R accept(Visitor<R> v) {
		return v.on(this);
	}
	
	//Throws an exception since Variable doesn't have a first operand
	public Expression getFirstOperand() {
		throw new UnsupportedOperationException();
	}
	
	//Throws an exception since Variable doesn't have a second operand
	public Expression getSecondOperand() {
		throw new UnsupportedOperationException();
	}
	
	/*
	 * Returns a string that represents the variable in a neat format, that is 
	 * used to display the final differentiated expression  
	 */
	public String toSimplifiedNotation() {
		return value;
	}

	/*
	 * Returns a simplified polynomial expression of a variable
	 * (non-Javadoc)
	 * @see differentiator.Expression#getSimplifiedPolynomial()
	 */
	@Override
	public SimplifiedPolynomial getSimplifiedPolynomial() {
		SimplifiedTerm simplifiedTerm = new SimplifiedTerm(1.0, value);
		SimplifiedPolynomial simplifiedPolynomial = new SimplifiedPolynomial(simplifiedTerm);
		return simplifiedPolynomial;
	}
}

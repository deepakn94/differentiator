package differentiator;

import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

public class SimplifiedTerm {
	double constant;
	Map<String, Integer> variables;
	
	/**
	 * Creates a new SimplifiedTerm object
	 * @param constant Constant coefficient associated with the term
	 * @param variable Variable associated with the term
	 */
	public SimplifiedTerm(double constant, String variable) {
		this.variables = new TreeMap<String, Integer> ();
		this.constant = constant;
		if (variable != null)
			this.variables.put(variable, 1);
	}
	
	/**
	 * Creates a new SimplifiedTerm object
	 * @param variables Map of variables contained in the SimplifiedTerm object
	 * @param constant Constant coeeficient associated with the term
	 */
	public SimplifiedTerm(Map<String, Integer> variables, double constant) {
		this.constant = constant;
		this.variables = variables;
	}

	
	public SimplifiedTerm(SimplifiedTerm term1, SimplifiedTerm term2) {
		double constant1 = term1.getConstant();
		double constant2 = term2.getConstant();
		constant = constant1 * constant2;
		
		Map<String, Integer> variables1 = term1.getVariables();
		Map<String, Integer> variables2 = term2.getVariables();
		
		variables = new TreeMap<String, Integer> ();
		
		for (Entry<String, Integer> entry : variables1.entrySet()) {
			String variable = entry.getKey();
			int exponent1 = entry.getValue();
			if (variables2.containsKey(variable)) {
				int exponent2 = variables2.get(variable);
				variables.put(variable, exponent1 + exponent2);
			} else {
				variables.put(variable, exponent1);
			}
		}
		for (Entry<String, Integer> entry : variables2.entrySet()) {
			String variable = entry.getKey();
			int exponent2 = entry.getValue();
			if (! variables1.containsKey(variable)) {
				variables.put(variable, exponent2);
			}
		}
	}
	
	@Override
	public String toString() {
		String termString = "";
		int count = 0;
		for (Entry<String, Integer> entry : variables.entrySet()) {
			if (count > 0)
				termString += "*";
			count++;
			int exponent = entry.getValue();
			if (exponent == 1) {
				termString += entry.getKey();
			} else {
				termString += (entry.getKey() + "^" + entry.getValue());
			}
			
		}
		return termString;
	}
	
	@Override
	public boolean equals(Object o) {
		if (! (o instanceof SimplifiedTerm)) 
			return false;
		SimplifiedTerm otherTerm = (SimplifiedTerm) o;
		Map<String, Integer> otherVariables = otherTerm.getVariables();
		if (variables.size() != otherVariables.size())
			return false;
		return this.toString().equals(otherTerm.toString());
	}
	
	public double getConstant() {
		return constant;
	}
	
	public Map<String, Integer> getVariables() {
		return variables;
	}
}

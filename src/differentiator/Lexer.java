package differentiator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import differentiator.Token.Type;

/**
 * A lexer takes a string and splits it into tokens that are meaningful to a
 * parser.
 */
public class Lexer {
    /**
     * Creates the lexer over the passed string.
     * @param string The string to tokenize.
     */
	
	private final String string;
	private int index = 0; //Keeps track of the index in the string reached by the matcher
	private final Matcher matcher;
	
	private static final Pattern REGEX 
		= Pattern.compile(
			"(\\()" + //Open parenthesis
			"|" + 
			"(\\))" + //Close parenthesis
			"|" +
			"([A-Za-z]+)" + //Variable name
			"|" +
			"([0-9]+\\.[0-9]*|[0-9]+)" + //Numerical constant
			"|" +
			"(\\*)" + //Operators - multiplication
			"|" +
			"(\\+)" //Operators - addition
		);
	
	private static final Type[] TOKEN_TYPE = 
	{
		Type.OPEN_PARENTHESIS,
		Type.CLOSE_PARENTHESIS,
		Type.VARIABLE,
		Type.CONSTANT,
		Type.MULTIPLY,
		Type.PLUS
	};
	
	/**
	 * Constructor of the class Lexer
	 * @param string
	 */
    public Lexer(String string) {
        this.string = string;
        this.matcher = REGEX.matcher(string);
    }
    
    /**
     * Resets the lexer, so that the lexer can run through the input expression again
     */
    public void reset() {
    	this.index = 0;
    }

    /**
     * Represents the next Token in the tokenized input expression; requires input expression
     * to be composed only of + and - symbols, integer and floating point numbers and variable 
     * names composed of letters only. Does not recognize any other symbols.
     * <p>
     * Throws an exception if input expression is in the wrong format. 
     * @return An object of the class Token. 
     */
    public Token next() {
    	
    	//If the end of the string is reached, append a new token of type "End of Expression"
    	if (index >= string.length())
    		return new Token(Type.EOE, "");
    	
    	if (! matcher.find(index)) {
    		throw new RuntimeException("Lexer exception");
    	}
    	
    	String newToken = matcher.group(0);
    	index = this.matcher.end(); //This moves the index forward
    	
    	for (int i=1; i<= matcher.groupCount(); ++i) {
    		if (matcher.group(i) != null) {
    			Type TokenType = TOKEN_TYPE[i-1];
    			return new Token(TokenType, newToken);
    		}
    	}
    	
    	//Should not reach here
    	throw new RuntimeException("Regex error - Should not reach here.");
    }
    
}

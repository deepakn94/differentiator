package differentiator;

import java.util.ArrayList;
import java.util.List;

public class SimplifiedPolynomial {
	List<SimplifiedTerm> simplifiedTerms;
	
	public SimplifiedPolynomial(SimplifiedTerm simplifiedTerm) {
		simplifiedTerms = new ArrayList<SimplifiedTerm> ();
		simplifiedTerms.add(simplifiedTerm);
	}
	
	public SimplifiedPolynomial(List<SimplifiedTerm> terms) {
		simplifiedTerms = terms;
	}
	
	public List<SimplifiedTerm> getTerms() {
		return simplifiedTerms;
	}
	
	/**
	 * Returns a SimplifiedPolynomial object which represents the sum of two polynomials
	 * @param simplifiedPolynomial Simplified Polynomial which is added to this
	 * @return A SimplifiedPolynomial object that represents the sum of the two polynomials
	 */
	public SimplifiedPolynomial add(SimplifiedPolynomial simplifiedPolynomial) {
		List<SimplifiedTerm> simplifiedTerms2 = simplifiedPolynomial.getTerms();
		List<SimplifiedTerm> newSimplifiedTerms = new ArrayList<SimplifiedTerm> ();
		for (SimplifiedTerm simplifiedTerm1 : simplifiedTerms) {
			boolean found = false;
			for (SimplifiedTerm simplifiedTerm2 : simplifiedTerms2) {
				if (simplifiedTerm1.toString().equals(simplifiedTerm2.toString())) {
					SimplifiedTerm simplifiedTerm = new SimplifiedTerm(simplifiedTerm1.getVariables(), simplifiedTerm1.getConstant() + simplifiedTerm2.getConstant());
					newSimplifiedTerms.add(simplifiedTerm);
					found = true;
				}
			}
			if (! found) {
				newSimplifiedTerms.add(simplifiedTerm1);
			}
		}
		
		for (SimplifiedTerm simplifiedTerm2 : simplifiedTerms2) {
			boolean found = false;
			for (SimplifiedTerm simplifiedTerm1 : simplifiedTerms) {
				if (simplifiedTerm1.toString().equals(simplifiedTerm2.toString())) {
					found = true;
				}
			}
			if (! found) {
				newSimplifiedTerms.add(simplifiedTerm2);
			}
		}
		return new SimplifiedPolynomial(newSimplifiedTerms);
	}
	
	/**
	 * Returns a SimplifiedPolynomial object which represents the product of two polynomials
	 * @param simplifiedPolynomial Simplified Polynomial which is multiplied to this
	 * @return A SimplifiedPolynomial object that represents the product of the two polynomials
	 */
	public SimplifiedPolynomial multiply(SimplifiedPolynomial multiplier) {
		List<SimplifiedTerm> simplifiedTerms2 = multiplier.getTerms();
		SimplifiedPolynomial simplifiedPolynomial = null;
		for (SimplifiedTerm simplifiedTerm1 : simplifiedTerms) {
			List<SimplifiedTerm> simplifiedTerms = new ArrayList<SimplifiedTerm> ();
			for (SimplifiedTerm simplifiedTerm2 : simplifiedTerms2) {
				SimplifiedTerm simplifiedTerm = new SimplifiedTerm(simplifiedTerm1, simplifiedTerm2);
				simplifiedTerms.add(simplifiedTerm);
			}
			if (simplifiedPolynomial == null) {
				simplifiedPolynomial = new SimplifiedPolynomial(simplifiedTerms);
			} else {
				SimplifiedPolynomial tempPolynomial = new SimplifiedPolynomial(simplifiedTerms);
				simplifiedPolynomial = simplifiedPolynomial.add(tempPolynomial);
			}
		}
		
		return simplifiedPolynomial;
	}
	
	@Override
	public String toString() {
		String finalString = "";
		int counter = 0;
		for (SimplifiedTerm simplifiedTerm : simplifiedTerms) {
			if (simplifiedTerm.constant != 0.0) {
				if (counter > 0)
					finalString += " + ";
				counter++;
				if (simplifiedTerm.toString() != "")
					finalString += (simplifiedTerm.constant + "*" + simplifiedTerm.toString());
				else
					finalString += simplifiedTerm.constant;
			}
		}
		return finalString;
	}
}

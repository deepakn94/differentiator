package differentiator;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import differentiator.Token.Type;

/*
 * Testing strategy -->
 * Check for normal functionality on a normally formatted expression involving variables and constants.
 * Check to ensure that multiple spaces do not distort results.
 * Check to ensure that extremely long variable names do not break the lexer.
 */

public class LexerTest {
	
	@Test
	public void testNext() {
		Lexer testLexer = new Lexer("(foo + 4.0) * mar");
		
		Type[] expectedTokenTypes = new Type[] {Type.OPEN_PARENTHESIS,
												Type.VARIABLE,
												Type.PLUS,
												Type.CONSTANT,
												Type.CLOSE_PARENTHESIS,
												Type.MULTIPLY,
												Type.VARIABLE};
		
		String[] expectedTokenValues = new String[] {"(",
													"foo",
													"+",
													"4.0",
													")",
													"*",
													"mar"};
		
		Token currentToken = testLexer.next();
		List<Type> actualTokenTypes = new ArrayList<Type> ();
		List<String> actualTokenValues = new ArrayList<String> ();
		while (currentToken.getTokentype()!=Type.EOE) {
			actualTokenTypes.add(currentToken.getTokentype());
			actualTokenValues.add(currentToken.getValue());
			currentToken = testLexer.next();
		}
		
		assertArrayEquals(expectedTokenTypes, actualTokenTypes.toArray());
		assertArrayEquals(expectedTokenValues, actualTokenValues.toArray());
	}
	
	@Test
	public void extraSpacesTest() {
		Lexer testLexer = new Lexer("(  a    *       4.34 )");
		
		Type[] expectedTokenTypes = new Type[] {Type.OPEN_PARENTHESIS,
												Type.VARIABLE,
												Type.MULTIPLY,
												Type.CONSTANT,
												Type.CLOSE_PARENTHESIS};
		
		String[] expectedTokenValues = new String[] {"(",
													"a",
													"*",
													"4.34",
													")"};
		
		Token currentToken = testLexer.next();
		List<Type> actualTokenTypes = new ArrayList<Type> ();
		List<String> actualTokenValues = new ArrayList<String> ();
		while (currentToken.getTokentype()!=Type.EOE) {
			actualTokenTypes.add(currentToken.getTokentype());
			actualTokenValues.add(currentToken.getValue());
			currentToken = testLexer.next();
		}
		
		assertArrayEquals(expectedTokenTypes, actualTokenTypes.toArray());
		assertArrayEquals(expectedTokenValues, actualTokenValues.toArray());
	}
	
	@Test
	public void extraLongVariables() {
		Lexer testLexer = new Lexer("(  abcdefghisfhsa)");
		
		Type[] expectedTokenTypes = new Type[] {Type.OPEN_PARENTHESIS,
												Type.VARIABLE,
												Type.CLOSE_PARENTHESIS};
		
		String[] expectedTokenValues = new String[] {"(",
													"abcdefghisfhsa",
													")"};
		
		Token currentToken = testLexer.next();
		List<Type> actualTokenTypes = new ArrayList<Type> ();
		List<String> actualTokenValues = new ArrayList<String> ();
		while (currentToken.getTokentype()!=Type.EOE) {
			actualTokenTypes.add(currentToken.getTokentype());
			actualTokenValues.add(currentToken.getValue());
			currentToken = testLexer.next();
		}
		
		assertArrayEquals(expectedTokenTypes, actualTokenTypes.toArray());
		assertArrayEquals(expectedTokenValues, actualTokenValues.toArray());
	}

}

package differentiator;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class DifferentiatorTest {

	/*
	 * Testing strategy -->
	 * Ensure that multiple parentheses do not affect the final output
	 * Ensure that expression involving multiple sums and products is differentiated correctly
	 * Check exceptions to make sure that expressions with too many or too few parentheses throw an exception
	 */
	@Test
	public void simpleVariableTest() {
		String input = "(x)";
		String variable = "x";
		String expectedOutput = "1";
		Differentiator differentiator = new Differentiator();
		assertEquals(expectedOutput, differentiator.evaluate(input, variable));
	}
	
	@Test
	public void simpleConstantTest() {
		String input = "(3.0)";
		String variable = "x";
		String expectedOutput = "0";
		Differentiator differentiator = new Differentiator();
		assertEquals(expectedOutput, differentiator.evaluate(input, variable));
	}
	
	@Test
	public void simpleDifferentVariableTest() {
		String input = "(y)";
		String variable = "x";
		String expectedOutput = "0";
		Differentiator differentiator = new Differentiator();
		assertEquals(expectedOutput, differentiator.evaluate(input, variable));
	}
	
	@Test
	public void simpleAdditionTest() {
		String input = "(x+x)";
		String variable = "x";
		String expectedOutput = "(1+1)";
		Differentiator differentiator = new Differentiator();
		assertEquals(expectedOutput, differentiator.evaluate(input, variable));
	}
	
	@Test
	public void simpleMultiplicationTest() {
		String input = "(x*x)";
		String variable = "x";
		String expectedOutput = "(x+x)";
		Differentiator differentiator = new Differentiator();
		assertEquals(expectedOutput, differentiator.evaluate(input, variable));
	}
	
	@Test
	public void simpleParenthesisTest() {
		String input = "((((x))))";
		String variable = "x";
		String expectedOutput = "1";
		Differentiator differentiator = new Differentiator();
		assertEquals(expectedOutput, differentiator.evaluate(input, variable));
	}
	
	@Test
	public void simpleCascadingParenthesisTest() {
		String input = "((36*yyyze)+4)";
		String variable = "yyyze";
		String expectedOutput = "36";
		Differentiator differentiator = new Differentiator();
		assertEquals(expectedOutput, differentiator.evaluate(input, variable));
	}
	
	@Test
	public void multipleExpressionEvaluateTest() {
		String input = "(((x*x)+4.0)*y)";
		String variable = "x";
		String expectedOutput = "((x+x)*y)";
		Differentiator differentiator = new Differentiator();
		assertEquals(expectedOutput, differentiator.evaluate(input, variable));
	}
	
	@Test
	public void constantEvaluateTest() {
		String input = "(x*y)";
		String variable = "y";
		String expectedOutput = "x";
		Differentiator differentiator = new Differentiator();
		assertEquals(expectedOutput, differentiator.evaluate(input, variable));
	}
	
	@Test(expected = RuntimeException.class)
	public void exceptionTooManyCloseParenthesesTest() {
		String input = "(x*y)))";
		String variable = "y";
		Differentiator differentiator = new Differentiator();
		differentiator.evaluate(input, variable);
	}
	
	@Test(expected = RuntimeException.class)
	public void exceptionTooManyOpenParenthesesTest() {
		String input = "(((x*y)";
		String variable = "y";
		Differentiator differentiator = new Differentiator();
		differentiator.evaluate(input, variable);
	}
	
	@Test(expected = RuntimeException.class)
	public void exceptionNoEnclosingParenthesesTest() {
		String input = "x*y";
		String variable = "y";
		Differentiator differentiator = new Differentiator();
		differentiator.evaluate(input, variable);
	}

}

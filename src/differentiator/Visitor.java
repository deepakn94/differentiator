package differentiator;

/**
 * This is the interface that the visitor class inherits from
 * @author Deepak
 *
 * @param <R>
 */
public interface Visitor<R> {
	public R on(Product prod);
	public R on(Sum sum);
	public R on(Variable var);
	public R on(Constant cons);
}

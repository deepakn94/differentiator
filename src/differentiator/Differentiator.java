package differentiator;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


/** Symbolic differentiator */
public class Differentiator {
    /**
     * Differentiates the passed expression string with respect to variable
     * and returns its derivative as a string.  If the expression or variable
     * is null, behavior is undefined.  If the expression is not parsable,
     * throws an exception.
     * @param expression The expression.
     * @param variable The variable to differentiate by.
     * @return The expression's derivative.
     */
    public String evaluate(String expression, String variable) {
    	Lexer lexer = new Lexer(expression);
    	Parser expressionParser = new Parser(lexer);
    	Expression inputExpression = expressionParser.returnExpression();
    	Expression result = differentiate(inputExpression, variable);
    	String finalExpression = result.getSimplifiedPolynomial().toString(); // Get simplified polynomial representation of the final expression
    	if (finalExpression.equals("")) {
    		finalExpression = "0.0";
    	}
    	//String finalExpression = result.toSimplifiedNotation();
        return finalExpression;
    }
    
    /**
	 * Returns the derivative of a polynomial function, which includes plus and into signs
	 * @param expression The expression that the user wants to differentiate
	 * @param variable The variable with respect to which we are differentiating
	 * @return An Expression object that represents the derivative of the expression wrt to the variable
	 */
    public static Expression differentiate(Expression expression, final String variable) {
    	return expression.accept(new Visitor<Expression>() {
    		public Expression on(Product prod) {
        		Expression operand1 = prod.getFirstOperand();
        		Expression operand2 = prod.getSecondOperand();
        		//Use the rule :- d/dx (uv) = u (dv/dx) + v (du/dx) 
        		return new Sum(new Product(differentiate(operand1, variable),operand2), new Product(operand1, differentiate(operand2, variable)));
        	}
        	
        	public Expression on(Sum sum) {
        		Expression operand1 = sum.getFirstOperand();
        		Expression operand2 = sum.getSecondOperand();
        		//Use the rule :- d/dx (u + v) = (dv/dx) + (du/dx)
        		return new Sum(differentiate(operand1, variable), differentiate(operand2, variable));
        	}
        	
        	public Expression on(Variable var) {
        		//Use the rule :- d/dx (v) = 0 if v!=x and d/dx (x) = 1 
        		if (variable.equals(var.getValue())) { return new Constant("1"); }
        		return new Constant("0");
        	}
        	
        	public Expression on(Constant cons) { return new Constant("0"); }
    	});
    }

    /**
     * Repeatedly reads expressions from the console, and outputs the results of
     * evaluating them. Inputting an empty line will terminate the program.
     * @param args unused
     */
    public static void main(String[] args) throws IOException {
        String result;

        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        String expression;
        do {
            // display prompt
            System.out.print("> ");
            // read input
            expression = in.readLine();
            // terminate if input empty
            if (!expression.equals("")) {
                try {
                    Differentiator diff = new Differentiator();
                    result = diff.evaluate(expression, "x");
                    System.out.println(result);
                } catch (RuntimeException re) {
                    System.err.println("Error: " + re.getMessage());
                }
            }
        } while (!expression.equals(""));
    }
}

package differentiator;

/**
 * Class Product that implements the interface Expression
 * <p>
 * Represents all products in an Expression
 * @author Deepak
 *
 */
public class Product implements Expression {
	private final Expression operand1, operand2;
	
	/**
	 * @param operand1 Object of type Expression; represents the first operand in the product
	 * @param operand2 Object of type Expression; represents the second operand in the product
	 */
	public Product(Expression operand1, Expression operand2) {
		this.operand1 = operand1;
		this.operand2 = operand2;
	}
	
	/*
	 * This method is used purely for the purpose of debugging/testing, because it gives a good idea
	 * of whether parsing is being done correctly or not
	 */
	@Override
	public String toString() {
		return "Product(" + operand1.toString() + "," + operand2.toString() + ")";
	}
	
	public <R> R accept(Visitor<R> v) {
		return v.on(this);
	}
	
	public Expression getFirstOperand() {
		return this.operand1;
	}
	
	public Expression getSecondOperand() {
		return this.operand2;
	}
	
	//Throws UnsupportedOperationException since Product doesn't have a value
	public String getValue() {
		throw new UnsupportedOperationException("Product does not have a value!");
	}
	
	/*
	 * Returns a string that represents the product in a neat format, that is 
	 * used to display the final differentiated expression  
	 */
	public String toSimplifiedNotation() {
		if (operand1.toSimplifiedNotation().equals("0") || operand2.toSimplifiedNotation().equals("0")) { return "0"; }
		if (operand1.toSimplifiedNotation().equals("1")) { return operand2.toSimplifiedNotation(); }
		if (operand2.toSimplifiedNotation().equals("1")) { return operand1.toSimplifiedNotation(); }
		
		StringBuilder result = new StringBuilder();
		result.append("(").append(operand1.toSimplifiedNotation()).append("*").append(operand2.toSimplifiedNotation()).append(")");
		return result.toString();
	}

	/*
	 * Returns a simplified polynomial expression for the product
	 * (non-Javadoc)
	 * @see differentiator.Expression#getSimplifiedPolynomial()
	 */
	@Override
	public SimplifiedPolynomial getSimplifiedPolynomial() {
		SimplifiedPolynomial polynomial1 = operand1.getSimplifiedPolynomial();
		SimplifiedPolynomial polynomial2 = operand2.getSimplifiedPolynomial();
		//TODO Implement the multiply method
		return polynomial1.multiply(polynomial2);
	}
}


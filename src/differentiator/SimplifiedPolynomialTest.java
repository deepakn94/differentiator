package differentiator;

import org.junit.Test;

public class SimplifiedPolynomialTest {

	@Test
	public void testAdd() {
		SimplifiedTerm simplifiedTerm1 = new SimplifiedTerm(3.0, "x");
		SimplifiedTerm simplifiedTerm2 = new SimplifiedTerm(4.0, "x");
		SimplifiedTerm simplifiedTerm3 = new SimplifiedTerm(5.0, "y");
		
		SimplifiedPolynomial simplifiedPolynomial1 = new SimplifiedPolynomial(simplifiedTerm1);
		SimplifiedPolynomial simplifiedPolynomial2 = new SimplifiedPolynomial(simplifiedTerm2);
		SimplifiedPolynomial simplifiedPolynomial3 = new SimplifiedPolynomial(simplifiedTerm3);
		
		System.out.println(simplifiedPolynomial1.multiply(simplifiedPolynomial2).add(simplifiedPolynomial3).multiply(simplifiedPolynomial3));
	}

}

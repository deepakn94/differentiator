package differentiator;

/**
 * Interface that prescribes what each element of an expression must do.
 * <p>
 * An expression is either a binary sum, a binary product, a variable or a constant (integer or float).
 * @author Deepak
 *
 */
public interface Expression {
	
	/**
	 * Method that returns a string representation of the given Expression that is readable. 
	 * This method is for debugging/testing purposes alone.
	 *
	 * <p>
	 * For example, an instance of the Product class is represented as the String "Product(a,b)"
	 * @return A String that represents the Expression object in an easy readable format
	 */
	public String toString();
	
	/**
	 * Method that returns the final string representation of the given Expression
	 * <p>
	 * For example, (Product(a,Sum(b,c)) is returned as (a*(b+c))
	 * @return A String that represents the final string representation of the given Expression
	 */
	public String toSimplifiedNotation();
	
	/**
	 * @return Returns the string value stored in the object
	 * Throws UnsupportedOperationException for Sum and Product objects
	 */
	public String getValue();
	
	/**
	 * Method that allows visitor into the class object
	 * @param v an object of the visitor class
	 * @return object of generic type R
	 */
	public <R> R accept(Visitor<R> v);
	
	/**
	 * @return The first operand in an Expression object
	 * Throws UnsupportedOperationException for Constant and Variable objects
	 */
	public Expression getFirstOperand();
	
	/**
	 * @return The second operand in an Expression object
	 * Throws UnsupportedOperationException for Constant and Variable objects
	 */
	public Expression getSecondOperand();
	
	/**
	 * @return Returns a simplified expression of the given Expression object
	 */
	public SimplifiedPolynomial getSimplifiedPolynomial();
}

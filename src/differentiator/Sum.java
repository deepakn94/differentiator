package differentiator;

/**
 * Class Sum that implements the interface Expression
 * <p>
 * Represents all sums in an Expression
 * @author Deepak
 *
 */
public class Sum implements Expression {
	private final Expression operand1, operand2;
	
	/**
	 * @param operand1 Object of type Expression; represents the first operand in the sum
	 * @param operand2 Object of type Expression; represents the second operand in the sum
	 */
	public Sum(Expression operand1, Expression operand2) {
		this.operand1 = operand1;
		this.operand2 = operand2;
	}
	
	/*
	 * This method is used purely for the purpose of debugging/testing, because it gives a good idea
	 * of whether parsing is being done correctly or not
	 */
	@Override
	public String toString() {
		return "Sum(" + operand1.toString() + "," + operand2.toString() + ")";
	}
	
	//Throws an exception since sum doesn't have a value
	public String getValue() {
		throw new UnsupportedOperationException("Sum does not have a value!");
	}
	
	public <R> R accept(Visitor<R> v) {
		return v.on(this);
	}

	public Expression getFirstOperand() {
		return this.operand1;
	}

	public Expression getSecondOperand() {
		return this.operand2;
	}
	
	/*
	 * Returns a string that represents the sum in a neat format, that is 
	 * used to display the final differentiated expression  
	 */
	public String toSimplifiedNotation() {
		if (operand1.toSimplifiedNotation().equals("0") && operand2.toSimplifiedNotation().equals("0")) { return "0"; }
		if (operand1.toSimplifiedNotation().equals("0")) { return operand2.toSimplifiedNotation(); }
		if (operand2.toSimplifiedNotation().equals("0")) { return operand1.toSimplifiedNotation(); }

		StringBuilder result = new StringBuilder();
		result.append("(").append(operand1.toSimplifiedNotation()).append("+").append(operand2.toSimplifiedNotation()).append(")");
		return result.toString();
	}
	
	/*
	 * Returns a simplified polynomial expression for a sum
	 * (non-Javadoc)
	 * @see differentiator.Expression#getSimplifiedPolynomial()
	 */
	@Override
	public SimplifiedPolynomial getSimplifiedPolynomial() {
		SimplifiedPolynomial polynomial1 = operand1.getSimplifiedPolynomial();
		SimplifiedPolynomial polynomial2 = operand2.getSimplifiedPolynomial();
		return polynomial1.add(polynomial2);
	}
}
